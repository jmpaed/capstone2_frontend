console.log("Hello from js");

let addCourseForm = document.querySelector("#createCourse");

addCourseForm.addEventListener("submit", (e) => {
  e.preventDefault();

let courseName = document.querySelector("#courseName").value;
let coursePrice = document.querySelector("#coursePrice").value;
let courseDescription = document.querySelector("#courseDescription").value;

  if(courseName !== " " && coursePrice !== " " && courseDescription !== " ") {
    fetch('https://fierce-savannah-99670.herokuapp.com/api/courses/course-exists', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: courseName
      })
    }).then(res => res.json()
      )
    .then(data => {
      if(data === false) {
        fetch("https://fierce-savannah-99670.herokuapp.com/api/courses/addCourse", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            name: courseName,
            price: coursePrice,
            description: courseDescription
          })
        }).then(res => {
          return res.json()
        }).then(data => {
          console.log(data);

          if(data === true) {
            Swal.fire({
              text:"New course added successfully.",
              icon: 'success',
              title: "Congratulations",
              footer: 'Skill Up Booking Services'
            })
          } else {
            Swal.fire({
              text: "Something went wrong.",
              icon: 'error',
              title: 'Woah there!',
              footer: 'Skill Up Booking Services'
          })
          };
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Duplicate Course!',
          text:"This course already exists in our system.",
          footer: 'Skill Up Booking Services'
        })
      }
    })
  } else {
    Swal.fire({
      icon: 'error',
      title: 'Invalid Entry',
      text: "Detected invalid entries. Please try again.",
      footer: 'Skill Up Booking Services'
  })
  }
});