//capture the html body w/c will display the content coming form the db
let modalButton = document.querySelector('#adminButton');
let container = document.querySelector('#coursesContainer');
// we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin");
let cardFooter;
if(isAdmin === "false" || !isAdmin){
    // if a user is a regular user, do not show the addcourse button.
    modalButton.innerHTML = null;
}else{
modalButton.innerHTML = `<div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a></div>`
}

fetch('https://fierce-savannah-99670.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
    console.log(data);

    let courseData;
    //create a control structure that will determine the value that the var courseData will hold
    if (data.length < 1) {
        courseData = "No course available"
    } else {
        //iterate the courses collection and display each course inside the browser
       courseData = data.map(course => {
            // let's check the make up of each element inside the courses collection
            console.log(course._id);

            // if the user is a regular user, display the enroll button and display course button.
            if(isAdmin == "false" || !isAdmin ) {
                cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course</a>

                
                `

            }else{
                cardFooter = `
                <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit </a>
                <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-warning text-white btn-block">Disable </a>
                <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block" id=deleteBtn>Delete </a>
                `
            }
            return (
                `
            <div class="col-md-6 my-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"> 
                        ${course.name}
                        </h5>
                        ${course.description}
                        <p class="card-text">
                        ${course.price}
                        </p>
                        ${course.createdOn}
                    </div>
                    <div class="card-footer">
                    	${cardFooter}
                    </div>
                </div>
            </div>
                ` // we attached a query string in the URL which allows us to embed the ID from the database record into the query string.
                // the ? inside the URL "acts" as a "separator", it indicates the end of a URL resource path, and indicates the start of the *query string*.
                // the # was originally used to jump to a specific element with the same id name/value.
            )
        }).join("") //use the join() to create a return of a new string
        //it contatenated all the objs in the array and converted each string into a data type
    }
    container.innerHTML = courseData;
})