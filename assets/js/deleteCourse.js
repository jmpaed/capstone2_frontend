console.log('delete');
let searchParams = new URLSearchParams(window.location.search);
let courseDelete = searchParams.get('courseId');
// console.log(...courseId);

console.log(searchParams.has('courseId'));

let token1 = localStorage.getItem('token');

// let deleteBtn = document.querySelector("#deleteBtn");
// 	deleteBtn.addEventListener("click", (e) => {
// 		e.preventDefault()
// 	})
console.log(token1);

fetch(`https://fierce-savannah-99670.herokuapp.com/api/courses/${courseDelete}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token1}`
        }
    }).then(res => {
        return res.json()
    }).then(data => {
        if(data === true) {
            Swal.fire({
                icon: 'info',
                title: 'Course Deleted!',
                text: `Course with id: ${courseDelete} has been deleted!`
            })
            window.location.replace('./courses.html');
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Uh-oh!',
                text: `Something went wrong!`
            })
        }
    })