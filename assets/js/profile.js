console.log('Hellow')

let token = localStorage.getItem("token")
console.log(token)

let profile = document.querySelector('#profileContainer')

// let's create a control structure that will determine the display if the access token is null or empty.
if(!token || token === null) {
	// lets redirect the user to the login page
	alert("You must be logged in.");
	window.location.href="./login.html"
}else{
	console.log("token is valid") //for educational purposes only
	fetch('https://fierce-savannah-99670.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data => {
		console.log(data);

		let enrollmentData = data.enrollments.map(classData => {
			console.log(classData)
			return(
				` 
				<tr id="profileTab">
					<td>${classData.courseId}</td>
					<td>${classData.enrolledOn}</td>
					<td>${classData.status}</td>
				</tr>
				`
				)
		}).join("") // remove the commas

		profile.innerHTML = 
		`
		<div class="col-md-12" id="profileView">
				<section class="jumbotron my-5" id="profileView2">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Course ID:</th>
								<th>Enrolled On:</th>
								<th>Status:</th>
								<tbody>${enrollmentData}</tbody>
							</tr>
						</thead>
					</table>
				</section>
		</div>	
		`
	})
}