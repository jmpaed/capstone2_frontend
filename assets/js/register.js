console.log("hello from JS file!")

let registerUserForm = document.querySelector('#registerUser');

registerUserForm.addEventListener("submit", (e) => {
    
    e.preventDefault();

    let fName = document.querySelector('#firstName').value
    let lName = document.querySelector('#lastName').value
    let uEmail = document.querySelector('#userEmail').value
    let mNumber = document.querySelector('#mobileNumber').value
    let pw1 = document.querySelector('#password1').value
    let pw2 = document.querySelector('#password2').value

    if ((pw1 === pw2) && (pw1 !== "" && pw2 !== "") && (mNumber.length === 11)) {
        fetch('https://fierce-savannah-99670.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: uEmail
            })
        }).then(res => res.json()
        ) // this will give the info if there are no dupes found.
        .then(data => {
            if(data === false){
                fetch("https://fierce-savannah-99670.herokuapp.com/api/users/register", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: uEmail,
                        mobileNo: mNumber,
                        password: pw1
                    })
                }).then(res => {
                    return res.json()
                }).then(data => {
                    console.log(data);
                    if(data === true) {
                        Swal.fire({
                                icon: 'success',
                                title: 'Congratulations',
                                text: "User has been successfully registered",
                                footer: 'Skill Up Booking Services'
                            })
                 } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Woah there!',
                        text: "Please check your input!",
                        footer: 'Skill Up Booking Services'
                    })
                 }
                }) 
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Existing Email!',
                    text: 'Choose another!',
                    footer: 'Skill Up Booking Services'
                })
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Woah there!',
            text: "Please check your input!",
            footer: 'Skill Up Booking Services'

        })
    }
})

// Individual work
// Include the SweetALERTextensions on the ff modules:
// 1. addCourse, Register, Login
// change all the default alert into Swal
// you can be creative in modifying the content of the alerts.
// required elements: icon, title, text & another key.