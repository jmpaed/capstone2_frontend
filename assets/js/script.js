console.log('Hellow from script.js')
// capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession')

// let's take the access token from the local storage property.

let userToken = localStorage.getItem("token")
console.log(userToken)

// let's create a control structure that will determine which elements inside the nav bar will be displayed if a user token is found in the local storage.

if(!userToken) {
	navItem.innerHTML =
	`	
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Log In</a>
	</li>
	<li class="nav-item">
		<a href="./register.html" class="nav-link">Register</a>
	</li>
	`
	
}else{
	navItem.innerHTML =
	`
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>
	`
}